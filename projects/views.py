from django.shortcuts import render
from django.views.generic import ListView
from .models import Project
from django.urls import path
from django.contrib.auth.views import LogoutView
from django.shortcuts import redirect
from django.http import HttpResponse
from django import forms

# Create your views here.

class ProjectListView(ListView):
    model = Project
    template_name = "project_list.html"
    My_Project = "projects"

def logout_view(request):
    return redirect("login")
urlpatterns = [
    path("logout/", logout_view, name = "logout")
]

class SighUpForm(forms.Form):
    username = forms.CharField(max_length=150)
    password = forms.CharField(max_length=150)
    password_confirmation = forms.CharField(max_length=150,widget=forms.PasswordInput)


def
