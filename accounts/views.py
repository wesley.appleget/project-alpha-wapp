from django.shortcuts import render
from django.contrib.auth import authenticate, login
from apps import LoginForm

# Create your views here.


def login_view(request):
    if request.method == "GET":
        form = LoginForm()

        user = authenticate(
            request,
        )
        return render(request, "login.html", "accounts/", {"form": form})
